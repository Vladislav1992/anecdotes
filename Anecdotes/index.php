<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Сайт анекдотов </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=4" type="text/css"> 
 </head>
 <body>
  <div class = "wrapper">
   <header class = "header">
	<ul>
	 <li> <a href = "/"> HOME </a></li>
	 <li> <a href = "#"> ABOUT US </a></li>
	 <li> <a href = "#"> CONTACTS </a></li>
	 <li> <a href = "forma.php"> SUGGEST ANECDOTE </a></li>
<?php
    if (!empty($_SESSION['auth'])){echo "<li><a href = \"admin\"> ADMIN </a></li><li><a href = \"admin/logout.php\"> LOGOUT </a></li>";} else {echo "<li> <a href = \"admin/login.php\"> ENTER AS ADMIN </a></li>";}    
?>	
	</ul>
   </header>
   <section class = "rightContent">
    <div class = "roster">
	 <form class = "rightform" action="" method="POST">
	  <select name="listAnecdotes">
<?php
$categories = [0 => 'Выберите вид анекдотов...', 1 => 'Анекдоты про корейцев', 2 => 'Анекдоты про русских', 3 => 'Анекдоты про Ржевского', 4 => 'Анекдоты про белорусов', 5 => 'Разные анекдоты(от пользователей)'];
for($i = 0; $i < count($categories); $i++){
echo "<option value=$i>$categories[$i]</option>";
}
//}
?>
      </select><br><br>
	  <input type="submit" value="Показать анекдоты!">
	 </form> 
	 <br>
	 </div>
	 <div class = "TheBest">
	  <p> Анекдот дня: </p>
	  <br>
	  <p>
<?php
   include('baza.php');

   $query = "SELECT * FROM about_vovochka ORDER BY RAND() LIMIT 5000";   
   $result = mysqli_query($link, $query) or die (mysqli_error($link));
   $text = mysqli_fetch_assoc($result);
   //var_dump($text);
   echo $text['text'];
?>
      </p>
	 </div>
   </section>
   <div class = "navigation">
    <nav>
	 <ul class="pagination">
<?php
   if(isset($_POST['listAnecdotes'])){
   if($_POST['listAnecdotes'] == 0){$_SESSION['list'] = 'about_koreans'; }
   if($_POST['listAnecdotes'] == 1){$_SESSION['list'] = 'about_koreans'; }
   if($_POST['listAnecdotes'] == 2){$_SESSION['list'] = 'about_russian'; }
   if($_POST['listAnecdotes'] == 3){$_SESSION['list'] = 'about_rzevsky'; }
   if($_POST['listAnecdotes'] == 4){$_SESSION['list'] = 'about_belorus'; }
   if($_POST['listAnecdotes'] == 5){$_SESSION['list'] = 'about_different'; }
   }
   
   $anecdot = 'about_koreans';
   
   if(!empty($_SESSION['list'])){
   $anecdot = $_SESSION['list'];}
   
   include('baza.php');
   
   if(isset($_POST['listAnecdotes'])){$_GET['page'] = 1;}
   
   if(isset($_GET['page'])){	
   $page = $_GET['page'];} else{
   $page = 1;}
   
   $notesOnPage = 8;
   $from = ($page - 1) * $notesOnPage;
   
   if($anecdot == 'about_different'){
   $query = "SELECT COUNT(*) as count FROM about_different WHERE status > 0";} else {	   
   $query = "SELECT COUNT(*) as count FROM $anecdot";}
   $result = mysqli_query($link, $query) or die(mysqli_error($link));
   $count = mysqli_fetch_assoc($result)['count'];
   $pagesCount = ceil($count / $notesOnPage);
   
   if($page != 1){
   $prev = $page - 1;
   echo 
   "<li>
     <a href=\"?page=$prev\"  aria-label=\"Previous\">
     <span aria-hidden=\"true\">&laquo;</span></a>
	</li>";
   }
   
   for ($i = 1; $i <= $pagesCount; $i++){
   if ($page == $i){
   echo "<li class=\"active\"><a href=\"?page=$i\">$i</a></li>";
   } else {
   echo "<li><a href=\"?page=$i\">$i</a></li>";
   }
   }
				  
   if($page != $pagesCount){
   $next = $page + 1;
   echo 
   "<li>
     <a href=\"?page=$next\"  aria-label=\"Next\">
     <span aria-hidden=\"true\">&raquo;</span></a>
    </li>";
    }  
?>	 
	 </ul>
	</nav>
   </div>
   <main>
<?php
   include('baza.php');
   
   if($anecdot == 'about_different'){
   $query = "SELECT text FROM about_different WHERE status > 0 LIMIT $from,$notesOnPage";} else {	   
   $query = "SELECT text FROM $anecdot LIMIT $from,$notesOnPage";}   
   $result = mysqli_query($link, $query) or die (mysqli_error($link));
   for ($data = [ ]; $row = mysqli_fetch_assoc($result); $data[ ] = $row);
   
   foreach($data as $elem){
   echo "<p>{$elem['text']}</p><br><br>";}
   //echo '<br>';
   //var_dump($_POST['listAnecdotes']);
   //echo $anecdot;
   //echo '<br>';
   //var_dump($_GET);
   //echo '<br>';
   //var_dump($_POST);
   //echo '<br>';
   //var_dump($_SESSION);
?>
   </main>
   <footer>
    <p><img src = "images/company.png" alt = "company"></p>
	<p>Copyright © 2001 - 2021  Anecdotes.yes</p>
   </footer>
   </div>
 </body>   
</html>   