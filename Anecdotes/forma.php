<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	
	session_start();
	
	//var_dump($_SESSION);
?>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Написать анекдот </title>
 <!--<link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
  <link rel = "stylesheet" href = "style.css?v=4" type="text/css"> 
 </head>
 <body>
  <div class = "wrapper">
    <header class = "header">
	<ul>
	 <li> <a href = "/"> HOME </a></li>
	 <li> <a href = "#"> ABOUT US </a></li>
	 <li> <a href = "#"> CONTACTS </a></li>
	 <li> <a href = "forma.php"> SUGGEST ANECDOTE </a></li>
<?php
    if (!empty($_SESSION['auth'])){echo "<li><a href = \"admin\"> ADMIN </a></li><li><a href = \"admin/logout.php\"> LOGOUT </a></li>";} else {echo "<li> <a href = \"admin/login.php\"> ENTER AS ADMIN </a></li>";}     
?>	
	</ul>
   </header>
   <p class = "mainformfirst">Здесь Вы можете предложить анекдот. Он появится в разделе "Разное" после проверки администратора.</p><br><br>
   <div class = "mainform">
    <div class = "succes">
<?php	
   include('baza.php');
   
   if(isset($_POST['suggest'])){
   $text = $_POST['suggest'];	   
   $query = "INSERT INTO about_different SET text='$text', status=0";   
   $result = mysqli_query($link, $query) or die (mysqli_error($link));
   echo $succes = 'анекдот успешно отправлен!';
   }   
?>
    </div>
    <form action="" method="POST">
     <textarea name = "suggest" placeholder = "Напишите здесь анекдот, а после нажмите Отправить"></textarea><br><br>
	 <input type="submit" value="Отправить анекдот">
    </form>
   </div>
<?php

?>   
   <footer>
    <p><img src = "images/company.png" alt = "company"></p>
	<p>Copyright © 2001 - 2021  Anecdotes.yes</p>
   </footer>
   </div>
 </body>   
</html>      